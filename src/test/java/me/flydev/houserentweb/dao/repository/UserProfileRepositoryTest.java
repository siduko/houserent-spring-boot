//package me.flydev.houserentweb.dao.repository;
//
//import me.flydev.houserentweb.config.RepositoryConfig;
//import me.flydev.houserentweb.dao.entity.UserProfile;
//import org.fest.assertions.Assertions;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import static org.junit.Assert.assertEquals;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = {RepositoryConfig.class})
//public class UserProfileRepositoryTest {
//
//    @Autowired
//    private UserProfileRepository userProfileRepository;
//
//    @Test
//    public void testUserProfile(){
//        //setup product
//        UserProfile userProfile = new UserProfile();
//        userProfile.setUserName("abc");
//        userProfile.setPassword("abc");
//
//        //save product, verify has ID value after save
//        Assertions.assertThat(userProfile.getId()).isNotNull(); //null before save
//        userProfileRepository.save(userProfile);
//        Assertions.assertThat(userProfile.getId()).isNotNull(); //not null after save
//
//        //fetch from DB
//        UserProfile fetchedUserProfile = userProfileRepository.findOne(userProfile.getId());
//
//        //should not be null
//        Assertions.assertThat(fetchedUserProfile).isNotNull();
//
//        //should equal
//        Assertions.assertThat(userProfile.getId()).isEqualTo(fetchedUserProfile.getId());
//        Assertions.assertThat(userProfile.getUserName()).isEqualTo(fetchedUserProfile.getUserName());
//
//        //update description and save
//        fetchedUserProfile.setUserName("New username");
//        userProfileRepository.save(fetchedUserProfile);
//
//        //get from DB, should be updated
//        UserProfile fetchedUpdatedUserProfile = userProfileRepository.findOne(fetchedUserProfile.getId());
//        Assertions.assertThat(fetchedUserProfile.getUserName()).isEqualTo(fetchedUpdatedUserProfile.getUserName());
//
//        //verify count of products in DB
//        long productCount = userProfileRepository.count();
//        Assertions.assertThat(productCount).isEqualTo(1);
//
//        //get all products, list should only have one
//        Iterable<UserProfile> userProfiles = userProfileRepository.findAll();
//
//        int count = 0;
//
//        for(UserProfile u : userProfiles){
//            count++;
//        }
//
//        assertEquals(count, 1);
//
//        userProfileRepository.delete(userProfile.getId());
//    }
//}