/**
 * Created by vuluan on 16/09/2015.
 */
angular.module('app.service',[])
.factory('ChatService', function ($log) {
        var socket = $.atmosphere;
        var request = {
            url: '/endpoint/chat',
            contentType: "application/json",
            logLevel: 'debug',
            transport: 'websocket',
            fallbackTransport: 'long-polling'
        };

        request.onOpen = function (response) {
            $log.info('Atmosphere connected using ' +response.transport);
        };

        request.onMessage = function (response) {
            var message = response.responseBody;
            $log.debug("Message receiver: "+message);
        };

        request.onError = function(response) {
            $log.error("Message error: "+response);
        };

        var subSocket = socket.subscribe(request);
        $('#btnSend').click(function () {
            var text = $('#chatInput').val();
            console.log("Message send: "+text)
            subSocket.push(text);
        });

        return {

        }

    });