package me.flydev.houserentweb.config;

import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages = "me.flydev.houserentweb.dao.repository")
@EntityScan(basePackages = "me.flydev.houserentweb.dao.entity")
@EnableTransactionManagement
public class RepositoryConfig {
}
