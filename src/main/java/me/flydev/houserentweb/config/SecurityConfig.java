package me.flydev.houserentweb.config;

import me.flydev.houserentweb.filter.AjaxConcurrentSessionFilter;
import me.flydev.houserentweb.handle.CustomAccessDeniedHandler;
import me.flydev.houserentweb.handle.CustomAuthenticationEntryPoint;
import me.flydev.houserentweb.handle.CustomAuthenticationFailureHandle;
import me.flydev.houserentweb.handle.CustomAuthenticationSuccessHandler;
import me.flydev.houserentweb.service.impl.CurrentUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationFilter;
import org.springframework.security.web.authentication.session.ConcurrentSessionControlAuthenticationStrategy;
import org.springframework.security.web.session.ConcurrentSessionFilter;

@Configuration
@EnableWebMvcSecurity
@EnableScheduling
@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    public static final String EXPIRED_URL = "/error/expiry";
    public static final String SUCCESS_REDIRECT_URL = "/";
    public static final String FAILURE_REDIRECT_URL = "/login?error";

    @Autowired
    private CurrentUserDetailsService currentUserDetailsService;
    @Autowired
    private CustomAccessDeniedHandler customAccessDeniedHandler;
    @Autowired
    private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(currentUserDetailsService)
                .passwordEncoder(new BCryptPasswordEncoder());
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
//                .addFilterAfter(ajaxConcurrentSessionFilter(), ConcurrentSessionFilter.class)
                .sessionManagement()
                .sessionAuthenticationStrategy(concurrentSessionControlStrategy())
                .maximumSessions(1)
                .expiredUrl(EXPIRED_URL);

        http.csrf().disable();
        http
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/rest/usermanagement/signup","/login","/endpoint/**").permitAll()
                .antMatchers("/rest/**").authenticated()
                .antMatchers("/admin/**").hasAnyRole("ROLE_ADMIN");
//                .anyRequest().authenticated();
        http
                .exceptionHandling()
                .authenticationEntryPoint(customAuthenticationEntryPoint)
                .accessDeniedHandler(customAccessDeniedHandler);
        http
                .formLogin()
                .successHandler(customAuthenticationSuccessHandler())
                .failureHandler(customAuthenticationFailureHandle());
        http
                .logout()
                .logoutUrl("/logout")
                .deleteCookies("remember-me")
                .logoutSuccessUrl("/")
                .permitAll();
        http
                .rememberMe()
                .tokenValiditySeconds(31536000);
    }

    @Bean
    public AuthenticationFailureHandler customAuthenticationFailureHandle() {
        AuthenticationFailureHandler authenticationFailureHandler = new CustomAuthenticationFailureHandle(FAILURE_REDIRECT_URL);
        return authenticationFailureHandler;
    }

    @Bean
    public AuthenticationSuccessHandler customAuthenticationSuccessHandler() {
        AuthenticationSuccessHandler authenticationSuccessHandler = new CustomAuthenticationSuccessHandler(SUCCESS_REDIRECT_URL);
        return authenticationSuccessHandler;
    }

    @Bean
    public SessionRegistryImpl sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public AjaxConcurrentSessionFilter ajaxConcurrentSessionFilter() {
        return new AjaxConcurrentSessionFilter(sessionRegistry(), EXPIRED_URL);
    }

    @Bean
    ConcurrentSessionControlAuthenticationStrategy concurrentSessionControlStrategy() {
        ConcurrentSessionControlAuthenticationStrategy concurrentSessionControlStrategy = new ConcurrentSessionControlAuthenticationStrategy(sessionRegistry());
        concurrentSessionControlStrategy.setMaximumSessions(1);
        return concurrentSessionControlStrategy;
    }
}
