package me.flydev.houserentweb.constant;

/**
 * Created by vuluan on 08/09/2015.
 */
public class StatusConstant {
    public static final String OK = "0";
    public static final String ERROR_PASSWORD_NOT_MATCH = "1002";
    public static String Error = "1";
    public static final String TECHNICAL_ERROR = "500";
    public static final String ERROR_REQUEST = "1000";
    public static final String ERROR_USER_EXIST = "1001";
}
