package me.flydev.houserentweb.constant;

public class HeaderAttribute {

    public static final String MOBILE_OS = "os";
    public static final String DEVICE_FINGER_PRINT = "devicefingerprint";
    public static final String DEVICE_TOKEN_ID = "devicetokenid";
    public static final String USER_KEY = "userkey";
    public static final String ACTION_KEY = "actionkey";

    public static final String PAYMENT_TYPE = "paymenttype";
    public static final String CURRENCY = "currency";
    public static final String LOCALE = "locale";
    public static final String REQUEST_URI = "RequestUri";
    public static final String REQUEST_URL = "http_requestUrl";
    public static final String PAYMENT_STATUS = "paymentstatus";
    public static final String VALIDATE_STATUS_HEADER = "validatestatus";
    public static final String VALIDATE_STATUS_VALUE_OK = "0";
    public static final String VALIDATE_STATUS_VALUE_NG = "1";
    public static final String LOCATION_COORDINATES = "locationcoordinates";

    public static final String COOKIE = "cookie";
    public static final String AUTHORIZATION = "authorization";
    public static final String SET_COOKIE = "Set-Cookie";
    public static final String TERMINAL_ID = "TerminalId";

    public static final String CLIENT_TRANSACTION_ID = "clienttransactionid";
    public static final String CLIENT_REFERENCE_ID = "clientreferenceid";
    public static final String CLIENT_TIMESTAMP = "clienttimestamp";


    public static final String PUSH_INFORMATION = "push-information";
    public static final String PAYMENT_TRANSACTION = "payment-transaction";
    public static final String SMARTID_CALLBACK_PAYMENT_TRANSACTION = "smartid-callback-payment-transaction";
    public static final String KYC_STATUS = "kyc-status";
    public static final String KYC_INFORMATION = "kyc-information";
    public static final String TOPUP_PAYMENT_TRANSACTION_HISTORY = "topup-payment-transaction-history";
    public static final String CALLING_CARD_TRANSACTION_HISTORY = "calling-card-transaction-history";
    public static final String SCAN_REFERENCE_ID = "scan-reference-id";
    public static final String FACE_MATCH = "face-match";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final String CLIENT_TIME_ZONE = "timeZone";
    public static final String APPLICATION_NAME = "applicationname";

    public static final String PASSWORD = "password";
    public static final String USER_ID = "user-id";
    public static final String PAYMENT_MODE = "payment-mode";
}
