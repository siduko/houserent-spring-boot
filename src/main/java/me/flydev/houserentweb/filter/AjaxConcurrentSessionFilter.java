package me.flydev.houserentweb.filter;

import me.flydev.houserentweb.util.WebUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by laivu on 08/09/2015.
 */
public class AjaxConcurrentSessionFilter extends GenericFilterBean {
    private final int AjaxSessionExpiredCode = 440;
    private SessionRegistry sessionRegistry;
    private LogoutHandler[] handlers = new LogoutHandler[] { new SecurityContextLogoutHandler() };
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
    private String expiredUrl;

    public AjaxConcurrentSessionFilter() {
        super();
    }

    public AjaxConcurrentSessionFilter(SessionRegistry sessionRegistry) {
        this.sessionRegistry = sessionRegistry;
    }

    public AjaxConcurrentSessionFilter(SessionRegistry sessionRegistry, String expiredUrl) {
        this.sessionRegistry = sessionRegistry;
        this.expiredUrl = expiredUrl;
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        javax.servlet.http.HttpSession session = request.getSession(false);

        if (session != null) {
            SessionInformation info = sessionRegistry.getSessionInformation(session.getId());

            if (info != null) {
                if (info.isExpired()) {
                    // Expired - abort processing
                    doLogout(request, response);
                    makeRedirection(request, response, info);
                    return;
                } else {
                    // Non-expired - update last request date/time
                    sessionRegistry.refreshLastRequest(info.getSessionId());
                }
            }
        }

        chain.doFilter(request, response);
    }

    private void makeRedirection(HttpServletRequest request, HttpServletResponse response, SessionInformation info) throws IOException {
        if (WebUtils.isAjaxRequest(request)) {
            response.setStatus(AjaxSessionExpiredCode);
        } else {
            String targetUrl = determineExpiredUrl(request, info);
            if (targetUrl != null) {
                redirectStrategy.sendRedirect(request, response, targetUrl);
                return;
            } else {
                response.getWriter().print("This session has been expired (possibly due to multiple concurrent " + "logins being attempted as the same user).");
                response.flushBuffer();
            }
        }

    }

    private void doLogout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        for (LogoutHandler handler : handlers) {
            handler.logout(request, response, auth);
        }
    }

    protected String determineExpiredUrl(HttpServletRequest request, SessionInformation info) {
        return expiredUrl;
    }
}
