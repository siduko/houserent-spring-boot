package me.flydev.houserentweb.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

public interface PushNotification {
    void push(List<String> deviceTokens, String message) throws UnsupportedEncodingException;
    void push(String deviceToken, String message) throws UnsupportedEncodingException;
}
