package me.flydev.houserentweb.service;

import me.flydev.houserentweb.dto.LocationDTO;

/**
 * Created by vuluan on 14/10/2015.
 */
public interface LocationService {
    Double getDistanceWith(LocationDTO local1, LocationDTO local2);
}
