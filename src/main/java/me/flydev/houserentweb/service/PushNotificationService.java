package me.flydev.houserentweb.service;


import me.flydev.houserentweb.dao.entity.DeviceType;

import java.io.UnsupportedEncodingException;
import java.util.List;

public interface PushNotificationService {
    void push(DeviceType device, List<String> deviceTokens, String message) throws UnsupportedEncodingException;
    void push(DeviceType device, String deviceToken, String message) throws UnsupportedEncodingException;
}
