package me.flydev.houserentweb.service;

import java.util.concurrent.ExecutionException;

/**
 * Created by joseph on 25/06/2015.
 */
public interface OTPService {
    String getOTP(String key) throws ExecutionException;
    void resetOTP(String key) throws ExecutionException;
}
