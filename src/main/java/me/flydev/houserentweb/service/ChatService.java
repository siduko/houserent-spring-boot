package me.flydev.houserentweb.service;

import me.flydev.houserentweb.dao.entity.UserProfile;
import me.flydev.houserentweb.dao.repository.UserProfileRepository;
import me.flydev.houserentweb.dto.ChatMessage;
import me.flydev.houserentweb.util.ChatMessageEncoderDecoder;
import me.flydev.houserentweb.util.JsonUtil;
import me.flydev.houserentweb.util.LOG;
import me.flydev.houserentweb.util.UserUtil;
import org.atmosphere.config.service.*;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.AtmosphereResourceEventListenerAdapter;
import org.atmosphere.cpr.MetaBroadcaster;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Provides a means for visitors to subscribe to "toast" messages.
 *
 * @author Andreas Kluth
 */
@Component
@ManagedService(path = "/endpoint/chat")
public class ChatService {

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private MetaBroadcaster metaBroadcaster;

    @Ready
    public void onReady(final AtmosphereResource resource) {
        LOG.info("ClientId [{}] connected.", resource.uuid());
        resource.addEventListener(new AtmosphereResourceEventListenerAdapter(){
            @Override
            public void onSuspend(AtmosphereResourceEvent event) {
                UserProfile userProfile = userProfileRepository.findOne(UserUtil.getUserId());
                userProfile.setIsOnline(true);
                userProfileRepository.save(userProfile);
                LOG.info("suspend");
            }

            @Override
            public void onHeartbeat(AtmosphereResourceEvent event) {
                LOG.info("heart");
            }

            @Override
            public void onDisconnect(AtmosphereResourceEvent event) {
                if(event.isCancelled())
                    LOG.info("Browser {} unexpectedly disconnected", event.getResource().uuid());
                else if(event.isClosedByClient())
                    LOG.info("Browser {} closed the connection", event.getResource().uuid());
                UserProfile userProfile = userProfileRepository.findOne(UserUtil.getUserId());
                userProfile.setIsOnline(false);
                userProfileRepository.save(userProfile);
            }

            @Override
            public void onBroadcast(AtmosphereResourceEvent event) {
                LOG.info("broadcast");
            }
        });

    }

    @Message(encoders = {ChatMessageEncoderDecoder.class},decoders = {ChatMessageEncoderDecoder.class})
    public ChatMessage onMessage(ChatMessage chatMessage){
        LOG.info("Message : [{}]", chatMessage.getContent());
        return chatMessage;
    }

    @Disconnect
    public void onDisconnect(AtmosphereResourceEvent event) {
        if(event.isCancelled())
            LOG.info("Browser outside {} unexpectedly disconnected", event.getResource().uuid());
        else if(event.isClosedByClient())
            LOG.info("Browser outside {} closed the connection", event.getResource().uuid());
    }


}
