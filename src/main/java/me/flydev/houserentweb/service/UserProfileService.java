package me.flydev.houserentweb.service;

import me.flydev.houserentweb.dto.CustomUserDetail;
import me.flydev.houserentweb.dto.LocationDTO;
import me.flydev.houserentweb.dto.request.LocationNearRequest;
import me.flydev.houserentweb.dto.request.PagingRequest;
import me.flydev.houserentweb.dto.response.UserProfileResponse;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

/**
 * Created by vuluan on 08/09/2015.
 */
public interface UserProfileService {
    CustomUserDetail findById(String userId);

    Page<CustomUserDetail> findAll(PagingRequest pagingRequest);

    CustomUserDetail findByUserName(String username);

    Double getDistanceWith(LocationDTO locationDTO);

    List<CustomUserDetail> findNear(LocationNearRequest locationNearRequest);

    LocationDTO getCurrentLocation();

    void setCurrentLocation(LocationDTO locationDTO);
}
