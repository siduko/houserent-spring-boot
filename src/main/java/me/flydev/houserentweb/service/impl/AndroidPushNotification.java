package me.flydev.houserentweb.service.impl;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Sender;
import me.flydev.houserentweb.service.PushNotification;
import me.flydev.houserentweb.util.LOG;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

@Service
public class AndroidPushNotification implements PushNotification {

    private static final int RETRIES = 1;
    private static final int TIME_TO_LIVE = 30;

    @Value("${android.push.api}")
    private String androidPushApiKey;

    @Override
    public void push(List<String> deviceTokens, String message) throws UnsupportedEncodingException {
        pushNotification(deviceTokens, message);
    }

    @Override
    public void push(String deviceToken, String message) throws UnsupportedEncodingException {
        pushNotification(Arrays.asList(deviceToken), message);
    }

    private void pushNotification(List<String> deviceTokens, String message) throws UnsupportedEncodingException {
        Message msg = createMessage(message);
        Sender sender = new Sender(androidPushApiKey);

        LOG.debug("Push Android device with token: {}, message: {}, api key: {}", deviceTokens, message, androidPushApiKey);
        try {
            sender.send(msg, deviceTokens, RETRIES);
        } catch (Exception e) {
            LOG.error("Cannot push alert message to Android devices " + e.getMessage(), e);
        }

    }

    private Message createMessage(String message) {
        String collapseKey = "collapseKey";
        Message.Builder mesaBuilder = new Message.Builder().collapseKey(collapseKey).timeToLive(TIME_TO_LIVE).delayWhileIdle(true);
        mesaBuilder.addData("message", message);
        return mesaBuilder.build();
    }
}
