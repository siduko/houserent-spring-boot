package me.flydev.houserentweb.service.impl;

import me.flydev.houserentweb.dao.entity.Role;
import me.flydev.houserentweb.dao.entity.UserProfile;
import me.flydev.houserentweb.dao.repository.UserProfileRepository;
import me.flydev.houserentweb.dto.CustomUserDetail;
import me.flydev.houserentweb.service.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by laivu on 08/09/2015.
 */
@Service
public class CurrentUserDetailsService implements UserDetailsService {

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserProfile userProfile = userProfileRepository.findOneByUsernameOrEmail(username);
        if(userProfile == null){
            throw new UsernameNotFoundException(String.format("User %s was not found", username));
        }
        CustomUserDetail userDetails = new CustomUserDetail(userProfile, getGrantedAuthorities(userProfile));
        return userDetails;
    }

    private Collection<? extends GrantedAuthority> getGrantedAuthorities(UserProfile userProfile) {
        Collection<SimpleGrantedAuthority> authorities = new HashSet<>();
        for (Role role : userProfile.getRoles()){
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return authorities;
    }
}
