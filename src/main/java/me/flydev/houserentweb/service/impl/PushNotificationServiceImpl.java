package me.flydev.houserentweb.service.impl;

import me.flydev.houserentweb.dao.entity.DeviceType;
import me.flydev.houserentweb.service.PushNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

@Service
public class PushNotificationServiceImpl implements PushNotificationService {

    @Autowired
    private IosPushNotification iosPushNotification;

    @Autowired
    private AndroidPushNotification androidPushNotification;

    @Override
    public void push(DeviceType device, List<String> deviceTokens, String message) throws UnsupportedEncodingException {
        switch (device) {
            case IOS:
                iosPushNotification.push(deviceTokens, message);
            case ANDROID:
                androidPushNotification.push(deviceTokens, message);
        }
    }

    @Override
    public void push(DeviceType device, String deviceToken, String message) throws UnsupportedEncodingException {
        push(device, Arrays.asList(deviceToken), message);
    }
}
