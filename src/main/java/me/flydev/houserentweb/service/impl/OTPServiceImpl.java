package me.flydev.houserentweb.service.impl;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import me.flydev.houserentweb.service.OTPService;
import me.flydev.houserentweb.util.OTPGenerator;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
public class OTPServiceImpl implements OTPService, InitializingBean {

    private LoadingCache<String, String> otpCache;

    @Value("${otp.duration}")
    private long duration;

    @Override
    public String getOTP(String key) throws ExecutionException {
        return otpCache.get(key);
    }

    @Override
    public void resetOTP(String key) throws ExecutionException {
        otpCache.invalidate(key);
    }

    private static String generate(String key) throws NoSuchAlgorithmException, InvalidKeyException {
        return OTPGenerator.generateOTP();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        otpCache = CacheBuilder.newBuilder()
                .maximumSize(Long.MAX_VALUE)
                .expireAfterWrite(getDuration(), TimeUnit.MILLISECONDS)
                .build(new CacheLoader<String, String>() {
                    @Override
                    public String load(String key) throws Exception {
                        return generate(key);
                    }
                });
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
