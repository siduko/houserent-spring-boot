package me.flydev.houserentweb.service.impl;

import me.flydev.houserentweb.constant.Metric;
import me.flydev.houserentweb.dao.entity.UserProfile;
import me.flydev.houserentweb.dao.repository.UserProfileRepository;
import me.flydev.houserentweb.dto.CustomUserDetail;
import me.flydev.houserentweb.dto.LocationDTO;
import me.flydev.houserentweb.dto.request.LocationNearRequest;
import me.flydev.houserentweb.dto.request.PagingRequest;
import me.flydev.houserentweb.dto.request.SortRequest;
import me.flydev.houserentweb.dto.response.UserProfileResponse;
import me.flydev.houserentweb.service.LocationService;
import me.flydev.houserentweb.service.UserProfileService;
import me.flydev.houserentweb.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by vuluan on 08/09/2015.
 */
@Service
public class UserProfileServiceImpl implements UserProfileService {

    @Autowired
    private UserProfileRepository userProfileRepository;
    @Autowired
    private LocationService locationService;

    @Override
    public CustomUserDetail findById(String userId) {
        UserProfile userProfile = userProfileRepository.findOne(userId);
        return getCustomUserDetail(userProfile);
    }

    @Override
    public Page<CustomUserDetail> findAll(PagingRequest pagingRequest) {
        PageRequest pageRequest = convertToPageRequest(pagingRequest);
        Page<UserProfile> profiles = userProfileRepository.findAll(pageRequest);
        return convertUserProfilesToUserProfilesRequest(profiles);
    }

    private Page<CustomUserDetail> convertUserProfilesToUserProfilesRequest(Page<UserProfile> profiles) {
        List<CustomUserDetail> userProfileResponseContent = new ArrayList<>();
        for (UserProfile userProfile : profiles.getContent()){
            userProfileResponseContent.add(getCustomUserDetail(userProfile));
        }
        Page<CustomUserDetail> userProfileResponses = new PageImpl<CustomUserDetail>(userProfileResponseContent);
        return userProfileResponses;
    }

    @Override
    public CustomUserDetail findByUserName(String username) {
        UserProfile profile = userProfileRepository.findOneByUsernameOrEmail(username);
        return getCustomUserDetail(profile);
    }

    @Override
    public Double getDistanceWith(LocationDTO locationDTO) {
        UserProfile userProfile = userProfileRepository.findOneByUsernameOrEmail(UserUtil.getUsername());
        LocationDTO locationDTO1 = new LocationDTO(userProfile.getLongitude(),userProfile.getLatitude());
        return locationService.getDistanceWith(locationDTO,locationDTO1);
    }

    @Override
    public List<CustomUserDetail> findNear(LocationNearRequest locationNearRequest) {
        LocationDTO currentLocation = getCurrentLocation();
        Metric metric = locationNearRequest.getMetric();
        double distance = locationNearRequest.getDistance();
        if(metric == Metric.Meter){
            distance = distance/1000.0;
        }
        List<UserProfile> userProfiles = userProfileRepository.findWithInByKilometer(distance,currentLocation.getLongitude(),currentLocation.getLatitude(),UserUtil.getUserId());

        return getUserDetails(userProfiles);
    }

    @Override
    public LocationDTO getCurrentLocation() {
        UserProfile userProfile = userProfileRepository.findOneByUsernameOrEmail(UserUtil.getUsername());
        return new LocationDTO(userProfile.getLongitude(),userProfile.getLatitude());
    }

    @Override
    public void setCurrentLocation(LocationDTO locationDTO) {
        UserProfile userProfile = userProfileRepository.findOneByUsernameOrEmail(UserUtil.getUsername());
        userProfile.setLongitude(locationDTO.getLongitude());
        userProfile.setLatitude(locationDTO.getLatitude());
        userProfileRepository.save(userProfile);
    }

    private List<CustomUserDetail> getUserDetails(List<UserProfile> userProfiles) {

        List<CustomUserDetail> userDetailses = new ArrayList<>();
        for (UserProfile userProfile : userProfiles){
            CustomUserDetail customUserDetail = getCustomUserDetail(userProfile);
            customUserDetail.eraseCredentials();
            userDetailses.add(customUserDetail);
        }
        return userDetailses;
    }

    private CustomUserDetail getCustomUserDetail(UserProfile userProfile) {
        return new CustomUserDetail(userProfile, Collections.<GrantedAuthority>emptyList());
    }

//    private CustomUserDetail convertUserProfileToUserProfileRequest(UserProfile profile) {
//        UserProfileResponse userProfileResponse = new UserProfileResponse();
//        userProfileResponse.setFirstName(profile.getFirstName());
//        userProfileResponse.setLastName(profile.getLastName());
//        userProfileResponse.setMiddleName(profile.getMiddleName());
//        userProfileResponse.setPhoneNumber(profile.getPhoneNumber());
//        return userProfileResponse;
//    }

    private PageRequest convertToPageRequest(PagingRequest pagingRequest) {
        SortRequest sortBy = pagingRequest.getSortBy();
        Sort.Direction direction = sortBy.isAcs() ? Sort.Direction.ASC : Sort.Direction.DESC;
        String field = sortBy.getField();
        Sort sort = new Sort(direction, field);
        PageRequest pageRequest = new PageRequest(pagingRequest.getPageOffset(),pagingRequest.getPageSize(), sort);
        return pageRequest;
    }
}
