package me.flydev.houserentweb.service.impl;

import me.flydev.houserentweb.dao.entity.Role;
import me.flydev.houserentweb.dao.entity.UserProfile;
import me.flydev.houserentweb.dao.repository.RoleRepository;
import me.flydev.houserentweb.dao.repository.UserProfileRepository;
import me.flydev.houserentweb.dto.request.SignUpRequest;
import me.flydev.houserentweb.dto.response.SignUpResponse;
import me.flydev.houserentweb.exception.PasswordNotMatchException;
import me.flydev.houserentweb.exception.UserExistException;
import me.flydev.houserentweb.service.UserManagementService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class UserManagementServiceImpl implements UserManagementService {
    @Autowired
    private UserProfileRepository userProfileRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public SignUpResponse signUp(SignUpRequest signUpRequest) {
        checkUserProfileExist(signUpRequest.getUsername());
        checkRepasswordMatch(signUpRequest.getPassword(),signUpRequest.getRepassword());

        UserProfile userProfile = convertSignUpRequestToUserProfile(signUpRequest);
        userProfileRepository.save(userProfile);

        SignUpResponse signUpResponse = new SignUpResponse();
        signUpResponse.setUsername(userProfile.getUserName());
        return signUpResponse;
    }

    private void checkRepasswordMatch(String password, String repasword) {
        if(!StringUtils.equals(password,repasword)){
            throw new PasswordNotMatchException();
        }
    }

    private UserProfile convertSignUpRequestToUserProfile(SignUpRequest signUpRequest) {
        UserProfile userProfile = new UserProfile();
        userProfile.setEmail(signUpRequest.getEmail());
        userProfile.setPassword(encryptPassword(signUpRequest.getPassword()));
        userProfile.setLastName(signUpRequest.getLastName());
        userProfile.setMiddleName(signUpRequest.getMiddleName());
        userProfile.setFirstName(signUpRequest.getFirstName());
        userProfile.setUserName(signUpRequest.getUsername());
        Role user = roleRepository.findOneByName("ROLE_USER");
        userProfile.setRoles(Arrays.asList(user));
        userProfile.setEnabled(true);
        return userProfile;
    }

    private void checkUserProfileExist(String username) {
        UserProfile userProfile = userProfileRepository.findOneByUsernameOrEmail(username);
        if(userProfile != null){
            throw new UserExistException(username);
        }
    }

    private String encryptPassword(String password) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder.encode(password);
    }
}
