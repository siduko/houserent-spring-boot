package me.flydev.houserentweb.service.impl;

import me.flydev.houserentweb.dto.LocationDTO;
import me.flydev.houserentweb.service.LocationService;
import org.springframework.stereotype.Service;

@Service
public class LocationServiceImpl implements LocationService {
    @Override
    public Double getDistanceWith(LocationDTO local1, LocationDTO local2) {
        long R = 6371000; // metres
        Double lat1 = local1.getLatitude();
        Double lat2 = local2.getLatitude();
        Double lon1 = local1.getLongitude();
        Double lon2 = local2.getLongitude();
        double φ1 = Math.toRadians(lat1);
        double φ2 = Math.toRadians(lat2);
        double Δφ = Math.toRadians(lat2 - lat1);
        double Δλ = Math.toRadians(lon2 - lon1);

        double a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
                Math.cos(φ1) * Math.cos(φ2) *
                        Math.sin(Δλ/2) * Math.sin(Δλ/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        return R * c;
    }
}
