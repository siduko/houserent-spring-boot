package me.flydev.houserentweb.service.impl;


import javapns.Push;
import javapns.communication.ProxyManager;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import me.flydev.houserentweb.service.PushNotification;
import me.flydev.houserentweb.util.LOG;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

@Service
public class IosPushNotification implements PushNotification {

    @Value("${ios.push.cert.file.path}")
    private String iosPushCertFilePath;

    @Value("${push.notification.is.production}")
    private boolean isPushNotificationOnProduction;

    @Value("${proxy.port}")
    private String proxyPort;

    @Value("${proxy.host}")
    private String proxyHost;

    @Value("${proxy.configuration.is.applied}")
    private boolean proxyEnabled;

    @Value("${ios.push.cert.password}")
    private String IosPushCertPassword;

    @Override
    public void push(List<String> deviceTokens, String message) throws UnsupportedEncodingException {
        pushNotification(deviceTokens, message);
    }

    @Override
    public void push(String deviceToken, String message) throws UnsupportedEncodingException {
        pushNotification(Arrays.asList(deviceToken), message);
    }

    private void pushNotification(List<String> deviceTokens, String message) throws UnsupportedEncodingException {
        if (proxyEnabled) {
            ProxyManager.setProxy(proxyHost, proxyPort);
        }

        try {
            Push.alert(message, iosPushCertFilePath, IosPushCertPassword, isPushNotificationOnProduction, deviceTokens);
            LOG.debug("Push IOS device with token: {}, message: {}, sslPath: {}, sslPassword: {}, Proxy: {}", deviceTokens, message, iosPushCertFilePath, IosPushCertPassword, proxyEnabled);

        } catch (CommunicationException | KeystoreException e) {
            LOG.error(" Cannot push alert message to IOS devices " + e.getMessage(), e);
        }

    }
}
