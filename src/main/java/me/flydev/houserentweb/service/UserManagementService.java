package me.flydev.houserentweb.service;

import me.flydev.houserentweb.dto.request.SignUpRequest;
import me.flydev.houserentweb.dto.response.SignUpResponse;

/**
 * Created by vuluan on 09/09/2015.
 */
public interface UserManagementService {
    SignUpResponse signUp(SignUpRequest signUpRequest);
}
