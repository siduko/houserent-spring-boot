package me.flydev.houserentweb.controller;

import me.flydev.houserentweb.constant.StatusConstant;
import me.flydev.houserentweb.dto.response.RestResponse;
import me.flydev.houserentweb.dto.response.ValidationErrorResponse;
import me.flydev.houserentweb.exception.PasswordNotMatchException;
import me.flydev.houserentweb.exception.UserExistException;
import me.flydev.houserentweb.util.LOG;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by vuluan on 08/09/2015.
 */
@ControllerAdvice
public class ErrorHandlingController {

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody RestResponse handleUncaughtException(Exception ex, WebRequest request, HttpServletResponse response) {
        LOG.info("Converting Uncaught exception to RestResponse : " + ex.getMessage());
        LOG.error(ex.getMessage(), ex);
        response.setHeader("Content-Type", "application/json");
        return new RestResponse(StatusConstant.TECHNICAL_ERROR, getMessage(StatusConstant.TECHNICAL_ERROR,null));
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody RestResponse handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request, HttpServletResponse response) {
        LOG.info("Converting IllegalArgumentException to RestResponse : " + ex.getMessage());
        LOG.error(ex.getMessage(), ex);
        response.setHeader("Content-Type", "application/json");
        return new RestResponse(StatusConstant.ERROR_REQUEST, ex.getMessage());
    }

    @ExceptionHandler(UserExistException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody RestResponse handleUserExistException(UserExistException ex, WebRequest request, HttpServletResponse response) {
        LOG.error(ex.getMessage(), ex);
        response.setHeader("Content-Type", "application/json");
        return new RestResponse(StatusConstant.ERROR_USER_EXIST, getMessage(StatusConstant.ERROR_USER_EXIST, new Object[]{ex.getUsername()}));
    }

    @ExceptionHandler(PasswordNotMatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody RestResponse handlePasswordNotMatchException(PasswordNotMatchException ex, WebRequest request, HttpServletResponse response) {
        LOG.error(ex.getMessage(), ex);
        response.setHeader("Content-Type", "application/json");
        return new RestResponse(StatusConstant.ERROR_PASSWORD_NOT_MATCH, getMessage(StatusConstant.ERROR_PASSWORD_NOT_MATCH,null));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody RestResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException ex,WebRequest request, HttpServletResponse response){
        LOG.error(ex.getMessage(), ex);
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        return new RestResponse(processFieldErrors(fieldErrors));
    }

    private ValidationErrorResponse processFieldErrors(List<FieldError> fieldErrors) {
        ValidationErrorResponse dto = new ValidationErrorResponse();

        for (FieldError fieldError: fieldErrors) {
            String localizedErrorMessage = resolveLocalizedErrorMessage(fieldError);
            dto.addFieldError(fieldError.getField(), localizedErrorMessage);
        }

        return dto;
    }

    private String resolveLocalizedErrorMessage(FieldError fieldError) {
        Locale currentLocale =  LocaleContextHolder.getLocale();
        String localizedErrorMessage = messageSource.getMessage(fieldError, currentLocale);
        if (localizedErrorMessage.equals(fieldError.getDefaultMessage())) {
            String[] fieldErrorCodes = fieldError.getCodes();
            localizedErrorMessage = fieldErrorCodes[0];
        }

        return localizedErrorMessage;
    }

    private String getMessage(String errorCode, Object[] errorArgs) {
        try {
            return messageSource.getMessage(errorCode, errorArgs, LocaleContextHolder.getLocale());
        } catch (Exception e) {
            LOG.error("Failed to get message for code{}", errorCode);
            return errorCode;
        }
    }
}
