package me.flydev.houserentweb.controller;

import org.atmosphere.cpr.MetaBroadcaster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by vuluan on 11/09/2015.
 */
@Controller
public class HomeController {

    @Autowired
    private MetaBroadcaster metaBroadcaster;
    @RequestMapping("/home")
    public String home(){
        metaBroadcaster.broadcastTo("/endpoint/chat","helllo");
        return "home";
    }
}
