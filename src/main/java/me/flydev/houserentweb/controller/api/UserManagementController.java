package me.flydev.houserentweb.controller.api;

import me.flydev.houserentweb.dto.CustomUserDetail;
import me.flydev.houserentweb.dto.request.PagingRequest;
import me.flydev.houserentweb.dto.request.SignUpRequest;
import me.flydev.houserentweb.dto.response.RestResponse;
import me.flydev.houserentweb.dto.response.SignUpResponse;
import me.flydev.houserentweb.dto.response.UserProfileResponse;
import me.flydev.houserentweb.service.UserManagementService;
import me.flydev.houserentweb.service.UserProfileService;
import me.flydev.houserentweb.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/rest/usermanagement/**")
public class UserManagementController {

    @Autowired
    private UserProfileService userProfileService;
    @Autowired
    private UserManagementService userManagementService;

    @RequestMapping(value = "/signup",method = RequestMethod.POST)
    public RestResponse<SignUpResponse> signup(@Valid @RequestBody SignUpRequest signUpRequest){
        SignUpResponse signUpResponse = userManagementService.signUp(signUpRequest);
        return new RestResponse<>(signUpResponse);
    }

    @RequestMapping(value = "/profile/all",method = RequestMethod.GET)
     public RestResponse<Page<CustomUserDetail>> findAll(@RequestBody PagingRequest pageRequest){
        return new RestResponse<>(userProfileService.findAll(pageRequest));
    }


    @RequestMapping(value = "/profile",method = RequestMethod.GET)
    public RestResponse<CustomUserDetail> findOne() {
        return new RestResponse<>(UserUtil.getUserDetails());
    }
}
