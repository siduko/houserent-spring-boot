package me.flydev.houserentweb.controller.api;

import me.flydev.houserentweb.dto.CustomUserDetail;
import me.flydev.houserentweb.dto.LocationDTO;
import me.flydev.houserentweb.dto.request.LocationNearRequest;
import me.flydev.houserentweb.dto.response.RestResponse;
import me.flydev.houserentweb.service.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest/location/**")
public class LocationController {
    @Autowired
    private UserProfileService userProfileService;

    @RequestMapping(method = RequestMethod.GET)
    public RestResponse<LocationDTO> getCurrentLocation(){
       return new RestResponse<>(userProfileService.getCurrentLocation());
    }

    @RequestMapping(method = RequestMethod.POST)
    public RestResponse<LocationDTO> setCurrentLocation(@RequestBody LocationDTO locationDTO){
        userProfileService.setCurrentLocation(locationDTO);
        return new RestResponse();
    }

    @RequestMapping(value = "/near",method = RequestMethod.POST)
    public RestResponse<List<CustomUserDetail>> getNear(@RequestBody LocationNearRequest locationNearRequest){
        return new RestResponse( userProfileService.findNear(locationNearRequest));
    }
}
