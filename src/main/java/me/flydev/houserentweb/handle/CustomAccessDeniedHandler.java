package me.flydev.houserentweb.handle;

import me.flydev.houserentweb.util.WebUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        if(WebUtils.isAjaxRequest(httpServletRequest)){
            httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }else {
            httpServletResponse.sendRedirect("/");
        }
    }
}
