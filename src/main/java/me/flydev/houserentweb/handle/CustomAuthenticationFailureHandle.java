package me.flydev.houserentweb.handle;

import me.flydev.houserentweb.util.WebUtils;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAuthenticationFailureHandle implements AuthenticationFailureHandler {

    private String redirectUrl;

    public CustomAuthenticationFailureHandle(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,AuthenticationException exception) throws IOException, ServletException {
        if(WebUtils.isAjaxRequest(request)){
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }else{
            response.sendRedirect(redirectUrl);
        }
    }
}
