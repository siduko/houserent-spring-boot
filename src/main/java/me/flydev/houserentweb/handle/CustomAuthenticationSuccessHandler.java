package me.flydev.houserentweb.handle;

import me.flydev.houserentweb.constant.HeaderAttribute;
import me.flydev.houserentweb.dao.entity.DeviceFingerPrint;
import me.flydev.houserentweb.dao.entity.DeviceType;
import me.flydev.houserentweb.dao.repository.DeviceFingerPrintRepository;
import me.flydev.houserentweb.dao.repository.UserProfileRepository;
import me.flydev.houserentweb.dto.DeviceFingerPrintModel;
import me.flydev.houserentweb.dto.request.SignUpRequest;
import me.flydev.houserentweb.util.JsonUtil;
import me.flydev.houserentweb.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Autowired
    private UserProfileRepository userProfileRepository;
    @Autowired
    private DeviceFingerPrintRepository deviceFingerPrintRepository;

    private String redirectUrl;

    public CustomAuthenticationSuccessHandler(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        if(WebUtils.isAjaxRequest(request)){
            response.setHeader("content-type","application/json");
            response.getWriter().write(JsonUtil.convertObjectToJson(authentication.getPrincipal()));
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
        }else{
            response.sendRedirect(redirectUrl);
        }
    }

    private DeviceFingerPrint findDeviceToken(HttpServletRequest request) {
        String deviceTokenId = request.getHeader(HeaderAttribute.DEVICE_TOKEN_ID);
        String os = request.getHeader(HeaderAttribute.MOBILE_OS);
        String deviceTokenInfo = request.getHeader(HeaderAttribute.DEVICE_FINGER_PRINT);
        DeviceFingerPrintModel deviceFingerPrintModel = JsonUtil.convertJsonToObject(deviceTokenInfo, DeviceFingerPrintModel.class);
        deviceFingerPrintModel.setOs(os);
        return storeAndGetDeviceFingerPrint(deviceFingerPrintModel, deviceTokenId);

    }

    public DeviceFingerPrint storeAndGetDeviceFingerPrint(DeviceFingerPrintModel deviceFingerPrintModel, String deviceTokenId) {
        DeviceFingerPrint deviceFingerPrint = deviceFingerPrintRepository.findByOsAndDeviceTokenId(deviceFingerPrintModel.getOs().toUpperCase(), deviceTokenId);
        if (deviceFingerPrint == null) {
            return createDeviceFingerPrint(deviceFingerPrintModel, deviceTokenId);
        }
        return deviceFingerPrint;
    }

    private DeviceFingerPrint createDeviceFingerPrint(DeviceFingerPrintModel requestedDeviceFingerPrint, String deviceTokenId) {

        DeviceFingerPrint deviceFingerPrint = new DeviceFingerPrint();
        deviceFingerPrint.setOs(DeviceType.valueOf(requestedDeviceFingerPrint.getOs()));
        deviceFingerPrint.setDeviceTokenId(deviceTokenId);
        deviceFingerPrint.setHandsetId(requestedDeviceFingerPrint.getHandsetID());
        deviceFingerPrint.setIccId(requestedDeviceFingerPrint.getIccID());
        deviceFingerPrint.setModelName(requestedDeviceFingerPrint.getModelName());
        deviceFingerPrint.setOem(requestedDeviceFingerPrint.getOem());
        deviceFingerPrint.setOsVersion(requestedDeviceFingerPrint.getOsVersion());
        deviceFingerPrint.setScreenHeight(requestedDeviceFingerPrint.getScreenHeight());
        deviceFingerPrint.setScreenWidth(requestedDeviceFingerPrint.getScreenWidth());
        return deviceFingerPrintRepository.save(deviceFingerPrint);
    }
}
