package me.flydev.houserentweb.exception;

/**
 * Created by vuluan on 09/09/2015.
 */
public class UserExistException extends RuntimeException {
    private String username;

    public UserExistException(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
