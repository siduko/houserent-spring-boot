package me.flydev.houserentweb.dto.request;

import me.flydev.houserentweb.constant.Metric;

import java.io.Serializable;

/**
 * Created by vuluan on 14/10/2015.
 */
public class LocationNearRequest implements Serializable {
    private static final long serialVersionUID = 7339784097881197826L;
    private double distance;
    private Metric metric;

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(long distance) {
        this.distance = distance;
    }
}
