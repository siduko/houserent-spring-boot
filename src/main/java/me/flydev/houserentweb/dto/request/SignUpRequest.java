package me.flydev.houserentweb.dto.request;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;

/**
 * Created by vuluan on 09/09/2015.
 */
public class SignUpRequest implements Serializable{
    private static final long serialVersionUID = 3116743099866231573L;

    @NotEmpty
    @Length(min = 6,max = 255)
    private String username;

    @NotEmpty
    @Length(min = 6,max = 255)
    private String password;

    @NotEmpty
    @Length(min = 6,max = 255)
    private String repassword;

    @NotEmpty
    @Length(max = 255)
    private String firstName;

    @NotEmpty
    @Length(max = 255)
    private String lastName;

    @NotEmpty
    @Length(max = 255)
    private String middleName;

    @NotEmpty
    private String phoneNumber;
    @NotEmpty
    @Email
    private String email;
    private String birthday;
    private String gender;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepassword() {
        return repassword;
    }

    public void setRepassword(String repassword) {
        this.repassword = repassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


}
