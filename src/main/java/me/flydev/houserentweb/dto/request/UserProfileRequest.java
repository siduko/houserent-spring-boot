package me.flydev.houserentweb.dto.request;

import java.io.Serializable;

/**
 * Created by vuluan on 10/09/2015.
 */
public class UserProfileRequest implements Serializable {
    private static final long serialVersionUID = 8956537073737328654L;

    private String firstName;
    private String lastName;
    private String middleName;
    private String phoneNumber;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
