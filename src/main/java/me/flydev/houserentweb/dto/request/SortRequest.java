package me.flydev.houserentweb.dto.request;

import java.io.Serializable;

public class SortRequest implements Serializable {
    private static final long serialVersionUID = 6507441051053947502L;
    private String field;
    private boolean acs;

    public SortRequest() {
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public boolean isAcs() {
        return acs;
    }

    public void setAcs(boolean acs) {
        this.acs = acs;
    }
}
