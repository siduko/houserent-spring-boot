package me.flydev.houserentweb.dto.request;

import java.io.Serializable;

public class PagingRequest implements Serializable{
    private static final long serialVersionUID = -2672485891674215617L;

    private int pageOffset;
    private int pageSize;
    private SortRequest sortBy;

    public PagingRequest() {
    }

    public int getPageOffset() {
        return pageOffset;
    }

    public void setPageOffset(int pageOffset) {
        this.pageOffset = pageOffset;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public SortRequest getSortBy() {
        return sortBy;
    }

    public void setSortBy(SortRequest sortBy) {
        this.sortBy = sortBy;
    }
}
