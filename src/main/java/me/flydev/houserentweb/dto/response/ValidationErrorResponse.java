package me.flydev.houserentweb.dto.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vuluan on 14/10/2015.
 */
public class ValidationErrorResponse implements Serializable {
    private static final long serialVersionUID = -5434282937969422238L;

    private List<FieldErrorResponse> fieldErrors = new ArrayList<>();


    public void addFieldError(String path, String message) {
        FieldErrorResponse error = new FieldErrorResponse(path, message);
        fieldErrors.add(error);
    }

    public List<FieldErrorResponse> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(List<FieldErrorResponse> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }
}
