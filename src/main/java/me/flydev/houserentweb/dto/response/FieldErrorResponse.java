package me.flydev.houserentweb.dto.response;

import java.io.Serializable;

/**
 * Created by vuluan on 14/10/2015.
 */
public class FieldErrorResponse implements Serializable{
    private static final long serialVersionUID = -1597190741056075385L;

    private String field;
    private String message;

    public FieldErrorResponse(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public String getMessage() {
        return message;
    }
}
