package me.flydev.houserentweb.dto.response;

import java.io.Serializable;

/**
 * Created by vuluan on 09/09/2015.
 */
public class SignUpResponse implements Serializable{
    private static final long serialVersionUID = -9140473736762514469L;

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
