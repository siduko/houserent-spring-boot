package me.flydev.houserentweb.dto.response;

import me.flydev.houserentweb.constant.StatusConstant;

import java.io.Serializable;

/**
 * Created by vuluan on 08/09/2015.
 */
public class RestResponse<T> implements Serializable{
    private static final long serialVersionUID = -2485187417049175449L;

    private String messageCode = StatusConstant.OK;
    private String messageInfo;
    private T body;

    public RestResponse() {
    }

    public RestResponse(String messageCode, String messageInfo) {
        this.messageCode = messageCode;
        this.messageInfo = messageInfo;
    }

    public RestResponse(T body) {
        this.body = body;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessageInfo() {
        return messageInfo;
    }

    public void setMessageInfo(String messageInfo) {
        this.messageInfo = messageInfo;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }
}
