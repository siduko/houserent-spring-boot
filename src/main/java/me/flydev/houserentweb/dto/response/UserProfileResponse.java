package me.flydev.houserentweb.dto.response;

import java.io.Serializable;

/**
 * Created by vuluan on 10/09/2015.
 */
public class UserProfileResponse implements Serializable {
    private static final long serialVersionUID = 6436856587261211064L;

    private String firstName;
    private String lastName;
    private String middleName;
    private String phoneNumber;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
