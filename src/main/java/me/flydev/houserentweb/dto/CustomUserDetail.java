package me.flydev.houserentweb.dto;

import me.flydev.houserentweb.dao.entity.UserProfile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class CustomUserDetail extends User implements Serializable {
    private static final long serialVersionUID = 4563647890727725045L;
    private String userId;
    private LocationDTO locationDTO;

    public CustomUserDetail(UserProfile userProfile, Collection<? extends GrantedAuthority> authorities) {
        super(userProfile.getEmail(), userProfile.getPassword(),
                userProfile.isEnabled(), userProfile.isAccountNonExpired(),
                userProfile.isCredentialsNonExpired(),
                userProfile.isAccountNonLocked(), authorities);
        this.userId = userProfile.getId();
        locationDTO = new LocationDTO(userProfile.getLongitude(),userProfile.getLatitude());
    }

    public String getUserId() {
        return userId;
    }

    public LocationDTO getLocationDTO() {
        return locationDTO;
    }

    public void setLocationDTO(LocationDTO locationDTO) {
        this.locationDTO = locationDTO;
    }
}
