package me.flydev.houserentweb.dto;

import java.io.Serializable;

/**
 * Created by vuluan on 14/10/2015.
 */
public class LocationDTO implements Serializable {
    private static final long serialVersionUID = 489041460073475711L;

    private Double longitude;
    private Double latitude;

    public LocationDTO(Double longitude, Double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public LocationDTO() {
        latitude = 0.0;
        latitude = 0.0;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
