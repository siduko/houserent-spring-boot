package me.flydev.houserentweb.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by vuluan on 11/09/2015.
 */
public class ChatMessage implements Serializable{
    private static final long serialVersionUID = -7714901934004349443L;

    private String userName;
    private String content;
    private long time = new Date().getTime();

    public ChatMessage(){
        this("", "");
    }

    public ChatMessage(String userName, String content) {
        this.userName = userName;
        this.content = content;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
