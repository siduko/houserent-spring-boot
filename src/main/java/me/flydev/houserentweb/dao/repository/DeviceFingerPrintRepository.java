package me.flydev.houserentweb.dao.repository;

import me.flydev.houserentweb.dao.entity.DeviceFingerPrint;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by vuluan on 14/10/2015.
 */
public interface DeviceFingerPrintRepository extends JpaRepository<DeviceFingerPrint,String>{
    DeviceFingerPrint findByOsAndDeviceTokenId(String var1, String var2);
}
