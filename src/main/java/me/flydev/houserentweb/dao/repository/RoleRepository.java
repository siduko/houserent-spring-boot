package me.flydev.houserentweb.dao.repository;

import me.flydev.houserentweb.dao.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by vuluan on 09/09/2015.
 */
public interface RoleRepository extends JpaRepository<Role,String>{
    Role findOneByName(String name);
}
