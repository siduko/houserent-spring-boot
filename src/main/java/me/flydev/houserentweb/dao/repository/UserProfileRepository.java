package me.flydev.houserentweb.dao.repository;

import me.flydev.houserentweb.dao.entity.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface UserProfileRepository extends JpaRepository<UserProfile,String> {
    @Query("select u from UserProfile u where u.userName=:value or u.email=:value and u.deleted = false")
    UserProfile findOneByUsernameOrEmail(@Param("value") String value);

    @Query(value = "SELECT *, ( 6371 * acos( cos( radians(:latitude) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(:longitude) ) + sin( radians(:latitude) ) * sin( radians( latitude ) ) ) ) AS distance FROM user_profile where id <> :userId HAVING distance < :distance ORDER BY distance LIMIT 0 , 20",nativeQuery = true)
    List<UserProfile> findWithInByKilometer(@Param("distance") double distance,@Param("longitude") double longitude,@Param("latitude") double latitude,@Param("userId") String userId);
}
