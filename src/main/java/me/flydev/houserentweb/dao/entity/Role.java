package me.flydev.houserentweb.dao.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/**
 * Created by vuluan on 08/09/2015.
 */
@Entity
@Table(name = "role")
public class Role implements Serializable{
    private static final long serialVersionUID = 8433167570595786052L;
    @Id
    private String id = UUID.randomUUID().toString();

    private String name;

    public Role() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
