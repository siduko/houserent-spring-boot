package me.flydev.houserentweb.dao.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by vuluan on 08/09/2015.
 */
@Entity
@Table(name = "user_profile")
public class UserProfile extends BaseEntity implements Serializable{
    private static final long serialVersionUID = 8592084599898317008L;
    @Id
    private String id = UUID.randomUUID().toString();

    @Column(name="account_non_expired")
    private boolean accountNonExpired = true;

    @Column(name="account_non_locked")
    private boolean accountNonLocked = true;

    @Column(name="attempt_password")
    private Integer attemptPassword = 0;

    @Column(name="attempt_reset_password")
    private Integer attemptResetPassword =0;

    private String birthday;

    @Column(name="calling_code")
    private String callingCode;

    @Column(name="country_code")
    private String countryCode;


    @Column(name="credentials_non_expired")
    private boolean credentialsNonExpired = true;

    private String email;

    private boolean enabled = false;

    @Column(name="first_name")
    private String firstName;

    private String gender;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="last_login_time")
    private Date lastLoginTime;

    @Column(name="last_name")
    private String lastName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="last_password_updated")
    private Date lastPasswordUpdated;

    @Lob
    @Column(name="last_passwords")
    private String lastPasswords;

    @Column(name="middle_name")
    private String middleName;

    private String password;

    @Column(name="phone_number")
    private String phoneNumber;

    @Column(name="reference_language")
    private String referenceLanguage = "en_US";

    @Column(name="user_name")
    private String userName;

    @Column(name="security_question")
    private String securityQuestion;

    @Column(name="security_answer")
    private String securityAnswer;

    @Column(name = "last_password_locked")
    private Date lastPasswordLocked;

    @Column(name = "verified_status")
    private boolean verifiedStatus = false;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "device_finger_print_id")
    private DeviceFingerPrint deviceFingerPrint;

    private Double longitude;
    private Double latitude;

    @Column(name = "is_online")
    private boolean isOnline;

    //bi-directional many-to-many association to Role
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name="user_role"
            , joinColumns={
            @JoinColumn(name="user_id")
    }
            , inverseJoinColumns={
            @JoinColumn(name="role_id")
    }
    )
    private List<Role> roles;

    public UserProfile() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public Integer getAttemptPassword() {
        return attemptPassword;
    }

    public void setAttemptPassword(Integer attemptPassword) {
        this.attemptPassword = attemptPassword;
    }

    public Integer getAttemptResetPassword() {
        return attemptResetPassword;
    }

    public void setAttemptResetPassword(Integer attemptResetPassword) {
        this.attemptResetPassword = attemptResetPassword;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCallingCode() {
        return callingCode;
    }

    public void setCallingCode(String callingCode) {
        this.callingCode = callingCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getLastPasswordUpdated() {
        return lastPasswordUpdated;
    }

    public void setLastPasswordUpdated(Date lastPasswordUpdated) {
        this.lastPasswordUpdated = lastPasswordUpdated;
    }

    public String getLastPasswords() {
        return lastPasswords;
    }

    public void setLastPasswords(String lastPasswords) {
        this.lastPasswords = lastPasswords;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getReferenceLanguage() {
        return referenceLanguage;
    }

    public void setReferenceLanguage(String referenceLanguage) {
        this.referenceLanguage = referenceLanguage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public Date getLastPasswordLocked() {
        return lastPasswordLocked;
    }

    public void setLastPasswordLocked(Date lastPasswordLocked) {
        this.lastPasswordLocked = lastPasswordLocked;
    }

    public DeviceFingerPrint getDeviceFingerPrint() {
        return deviceFingerPrint;
    }

    public void setDeviceFingerPrint(DeviceFingerPrint deviceFingerPrint) {
        this.deviceFingerPrint = deviceFingerPrint;
    }

    public String getSecurityQuestion() {
        return securityQuestion;
    }

    public void setSecurityQuestion(String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }

    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }

    public boolean isVerifiedStatus() {
        return verifiedStatus;
    }

    public void setVerifiedStatus(boolean verifiedStatus) {
        this.verifiedStatus = verifiedStatus;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setIsOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }
}
