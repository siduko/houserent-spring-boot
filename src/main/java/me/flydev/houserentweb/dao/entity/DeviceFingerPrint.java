package me.flydev.houserentweb.dao.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

/**
 * Created by vuluan on 08/09/2015.
 */
@Entity
@Table(name = "device_finger_print")
public class DeviceFingerPrint extends BaseEntity implements Serializable{

    private static final long serialVersionUID = 1353469177497062675L;
    @Id
    private String id = UUID.randomUUID().toString();
    @Column(name = "device_token_id")
    private String deviceTokenId;
    @Column
    DeviceType os;
    @Column(name = "handset_id")
    private String handsetId;

    @Column(name = "icc_id")
    private String iccId;

    @Column(name = "model_name")
    private String modelName;

    @Column(name = "model_no")
    private String modelNo;

    private String oem;

    @Column(name = "os_version")
    private String osVersion;

    @Column(name = "screen_height")
    private Integer screenHeight;

    @Column(name = "screen_width")
    private Integer screenWidth;

    public DeviceFingerPrint() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHandsetId() {
        return handsetId;
    }

    public void setHandsetId(String handsetId) {
        this.handsetId = handsetId;
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getOem() {
        return oem;
    }

    public void setOem(String oem) {
        this.oem = oem;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public Integer getScreenHeight() {
        return screenHeight;
    }

    public void setScreenHeight(Integer screenHeight) {
        this.screenHeight = screenHeight;
    }

    public Integer getScreenWidth() {
        return screenWidth;
    }

    public void setScreenWidth(Integer screenWidth) {
        this.screenWidth = screenWidth;
    }

    public String getDeviceTokenId() {
        return deviceTokenId;
    }

    public void setDeviceTokenId(String deviceTokenId) {
        this.deviceTokenId = deviceTokenId;
    }

    public DeviceType getOs() {
        return os;
    }

    public void setOs(DeviceType os) {
        this.os = os;
    }
}
