package me.flydev.houserentweb.dao.entity;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@MappedSuperclass
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = -2106801805465239762L;
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date created = Calendar.getInstance().getTime();
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated")
    @LastModifiedDate
    private Date lastUpdated = Calendar.getInstance().getTime();
    @Column(name = "version_no")
    @Version
    private Integer versionNo = 0;
    private boolean deleted = false;

    public void updateLastUpdated() {
        lastUpdated = Calendar.getInstance().getTime();
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Integer getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(Integer versionNo) {
        this.versionNo = versionNo;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

}
