package me.flydev.houserentweb.dao.entity;

public enum DeviceType {
    IOS
    , ANDROID
}
