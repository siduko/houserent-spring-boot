package me.flydev.houserentweb.util;

import javax.crypto.spec.DESedeKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class OTPGenerator {

    private static final int CODE_DIGITS = 6;
    private static final int TRUNCATION_OFFSET = 3;

    public static String generateOTP() throws InvalidKeyException, NoSuchAlgorithmException {
        byte[] code = generateSharedSecret();
        String opt = HOTPAlgorithm.generateOTP(code, System.currentTimeMillis()/10000, CODE_DIGITS, false, TRUNCATION_OFFSET);
        return opt;
    }

    private static byte[] generateSharedSecret() throws NoSuchAlgorithmException {
        SecureRandom random=SecureRandom.getInstance("SHA1PRNG");
        random.setSeed(System.currentTimeMillis());
        byte bytes[]=new byte[DESedeKeySpec.DES_EDE_KEY_LEN];
        random.nextBytes(bytes);
        return bytes;
    }
}
