package me.flydev.houserentweb.util;

import me.flydev.houserentweb.dto.ChatMessage;
import org.atmosphere.config.managed.Decoder;
import org.atmosphere.config.managed.Encoder;

public final class ChatMessageEncoderDecoder implements Encoder<ChatMessage,String>, Decoder<String,ChatMessage> {
    @Override
    public ChatMessage decode(String s) {
        return JsonUtil.convertJsonToObject(s,ChatMessage.class);
    }

    @Override
    public String encode(ChatMessage chatMessage) {
        return JsonUtil.convertObjectToJson(chatMessage);
    }
}
