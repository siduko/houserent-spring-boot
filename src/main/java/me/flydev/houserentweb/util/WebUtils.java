package me.flydev.houserentweb.util;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class WebUtils {


    public static boolean isAjaxRequest(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }

    public static String removeLeadingZero(String leadingZeroString) {
        leadingZeroString = leadingZeroString.trim();
        return leadingZeroString.replaceAll("^0+", "");
    }


    public static HttpServletRequest getCurrentRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    public static boolean isCurrentUserAuthenticated() {
        return !WebUtils.isAnonymousUser() && SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
    }

    public static boolean isRoleUser() {
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for (GrantedAuthority grantedAuthority : authorities) {
            if ("ROLE_USER".equals(grantedAuthority.toString())) {
                return true;
            }
        }
        return false;
    }

    public static void forceLogout(HttpServletRequest request, HttpServletResponse response) {
        LogoutHandler[] handlers = new LogoutHandler[]{new SecurityContextLogoutHandler()};
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        for (LogoutHandler handler : handlers) {
            handler.logout(request, response, auth);
        }
    }

    public static boolean isAnonymousUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication instanceof AnonymousAuthenticationToken;
    }

    public static void copyRedirectAttributeToModel(RedirectAttributes redirectAttributes, ModelMap model) {
        model.addAllAttributes(redirectAttributes.getFlashAttributes());
    }

    public static void copyModelToRedirectAttribute(RedirectAttributes redirectAttributes, ModelMap model) {
        Set<Map.Entry<String, Object>> entries = model.entrySet();
        for (Map.Entry<String, Object> entry : entries) {
            redirectAttributes.addFlashAttribute(entry.getKey(), entry.getValue());
        }
    }

    public static void addToModelIfEmpty(ModelMap model, String attribute, Object object) {
        if (!model.containsKey(attribute)) {
            model.addAttribute(attribute, object);
        }
    }
}