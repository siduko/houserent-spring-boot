package me.flydev.houserentweb.util;

import me.flydev.houserentweb.dto.CustomUserDetail;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Created by vuluan on 14/10/2015.
 */
public class UserUtil {
    public static String getUsername(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }
    public static CustomUserDetail getUserDetails(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (CustomUserDetail)authentication.getPrincipal();
    }
    public static String getUserId(){
        return getUserDetails().getUserId();
    }
}
